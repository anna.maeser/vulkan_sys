#[cfg(target_family = "unix")]
mod unix {
    pub use wayland_client::protocol::wl_display::WlDisplay as wl_display;
    pub use wayland_client::protocol::wl_surface::WlSurface as wl_surface;

    pub use x11_dl::xlib::{Display, VisualID, Window};
}

#[cfg(target_family = "unix")]
pub use self::unix::*;

#[cfg(target_family = "windows")]
mod windows {
    pub use winapi::minwinbase::SECURITY_ATTRIBUTES;
    pub use winapi::minwindef::{DWORD, HINSTANCE};
    pub use winapi::windef::HWND;
    pub use winapi::winnt::{HANDLE, LPCWSTR};
}

#[cfg(target_family = "windows")]
pub use self::windows::*;
