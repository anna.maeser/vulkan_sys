use vk::ClearDepthStencilValue;

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct ClearColorValue([u32; 4]);

impl ClearColorValue {
    #[inline]
    pub fn as_f32(&self) -> &[f32; 4] {
        use std::mem;
        unsafe { mem::transmute(&self.0) }
    }

    #[inline]
    pub fn as_i32(&self) -> &[i32; 4] {
        use std::mem;
        unsafe { mem::transmute(&self.0) }
    }

    #[inline]
    pub fn as_u32(&self) -> &[u32; 4] {
        use std::mem;
        unsafe { mem::transmute(&self.0) }
    }

    #[inline]
    pub fn from_f32(value: [f32; 4]) -> Self {
        use std::mem;
        ClearColorValue(unsafe { mem::transmute(value) })
    }

    #[inline]
    pub fn from_i32(value: [i32; 4]) -> Self {
        use std::mem;
        ClearColorValue(unsafe { mem::transmute(value) })
    }

    #[inline]
    pub fn from_u32(value: [u32; 4]) -> Self {
        use std::mem;
        ClearColorValue(unsafe { mem::transmute(value) })
    }
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct ClearValue(ClearColorValue);

impl ClearValue {
    #[inline]
    pub fn as_color(&self) -> &ClearColorValue {
        &self.0
    }

    #[inline]
    pub fn as_depth_stencil(&self) -> &ClearDepthStencilValue {
        use std::mem;
        unsafe { mem::transmute(&self.0) }
    }

    #[inline]
    pub fn from_color(value: ClearColorValue) -> Self {
        ClearValue(value)
    }

    #[inline]
    pub fn from_depth_stencil(value: ClearDepthStencilValue) -> Self {
        use std::mem;
        let value = (value, [0u32, 0u32]);
        ClearValue(unsafe { mem::transmute(value) })
    }
}
