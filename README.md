# vulkan_sys

Rust bindings to the the [Vulkan&#174;] graphics API generated from the official API registry ([Vulkan-Docs] on GitHub).

Note that vulkan\_sys does **not** require you to install the official Vulkan SDK. This is not something specific to vulkan\_sys. It is only required to have a Vulkan capable driver installed which includes the loader library.

[Vulkan&#174;]: https://www.khronos.org/vulkan
[Vulkan-Docs]: https://github.com/KhronosGroup/Vulkan-Docs

## License

`vulkan_sys` is primarily distributed under the terms of both the MIT license and the Apache License (Version 2.0), with portions covered by various BSD-like licenses.

See [LICENSE-APACHE.md] and [LICENSE-MIT.md] for details.

[LICENSE-APACHE.md]: LICENSE-APACHE.md
[LICENSE-MIT.md]: LICENSE-MIT.md
