#![feature(box_syntax)]
#![feature(conservative_impl_trait)]

#[macro_use]
extern crate lazy_static;
extern crate inflector;
extern crate regex;
extern crate sxd_document;
extern crate sxd_xpath;

mod utils;
mod parse;
mod generate;

use parse::Data;
use std::env;
use std::fs::File;
use std::path::Path;
use sxd_document::Package;
use sxd_document::dom::Document;

pub fn main() {
    update();
    generate();
}

/// Updates the git submodules.
fn update() {
    use std::process::Command;

    let status = Command::new("git")
        .arg("submodule")
        .arg("update")
        .status()
        .expect("git submodule update");
    assert!(status.success(), "git submodule update");
}

/// Generates the API bindings.
fn generate() {
    let defines = get_defines();

    let xml = env::var("CARGO_MANIFEST_DIR").unwrap();
    let xml = Path::new(&xml).join("Vulkan-Docs/src/spec/vk.xml");
    let xml = parse::read_xml(xml.to_str().unwrap());

    let out_path = env::var("OUT_DIR").unwrap();
    let out_path = Path::new(&out_path).join("vk.generated.rs");
    let ctx_extract = ContextExtract { defines: defines.clone(), xml: &xml };
    let ctx_generate = ContextGenerate { out_path: out_path.to_str().unwrap(), data: parse::extract_data(&ctx_extract) };

    let mut target = Target {
        file: File::create(ctx_generate.out_path).expect(&format!("create {}", ctx_generate.out_path)),
    };

    generate::generate(&ctx_generate, &mut target);
}

/// Get all the defined names used to check whether parts of the API are to be generated or not.
fn get_defines() -> Vec<String> {
    let target_family = env::var("CARGO_CFG_TARGET_FAMILY").unwrap();

    let mut defines = Vec::new();
    match target_family.as_ref() {
        "unix" => {
            defines.push("VK_USE_PLATFORM_WAYLAND_KHR".to_owned());
            defines.push("VK_USE_PLATFORM_XLIB_KHR".to_owned());
        }
        "windows" => {
            defines.push("VK_USE_PLATFORM_WIN32_KHR".to_owned());
            defines.push("VK_USE_PLATFORM_WIN32_KHX".to_owned());
        }
        _ => (),
    }
    defines
}

/// Extraction context.
pub struct ContextExtract<'a> {
    defines: Vec<String>,
    xml: &'a Package,
}

impl<'a> ContextExtract<'a> {
    fn document(&self) -> Document {
        self.xml.as_document()
    }
}

/// Generation context.
///
/// Contains all necessary information required to generate the API binding.
pub struct ContextGenerate<'a> {
    out_path: &'a str,
    data: Data<'a>,
}

/// Genenration target.
///
/// Contains the necessary facilities for writig the generated contetent.
pub struct Target {
    file: File,
}
