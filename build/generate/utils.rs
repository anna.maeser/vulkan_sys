use ContextGenerate;
use parse::Data;
use parse::basic::Variant;
use parse::en::Enum;
use parse::tag::Tag;
use utils::*;

/// Checks whether a type is not available due to extension definitions.
pub fn check_unavailable_type(ctx: &ContextGenerate, ty: &str) -> bool {
    ctx.data
        .extensions
        .iter()
        .filter(|extension| !extension.available)
        .flat_map(|extension| extension.required_types.iter())
        .any(|&required| required == ty)
}

/// Checks whether a command is not available due to extension definitions.
pub fn check_unavailable_command(ctx: &ContextGenerate, command: &str) -> bool {
    ctx.data
        .extensions
        .iter()
        .filter(|extension| !extension.available)
        .flat_map(|extension| extension.required_commands.iter())
        .any(|&required| required == command)
}

/// Merges variants from an enumeration and extensions.
pub fn merge_variants<'a>(ctx: &'a ContextGenerate, en: &'a Enum<'a>) -> Vec<&'a Variant<'a>> {
    let mut merged = Vec::new();

    merged.extend(en.variants.iter());

    merged.extend(
        ctx.data
            .extensions
            .iter()
            .filter(|extension| extension.available)
            .flat_map(|extension| extension.required_variants.iter())
            .filter(|variant| variant.extends.iter().filter(|&&extend| extend == en.name).next().is_some()),
    );

    merged
}

/// Checks if a value is ending with a defined tag which then is returned.
pub fn find_tag<'a>(value: &str, data: &'a Data<'a>) -> Option<&'a Tag<'a>> {
    for tag in TAGS {
        if value.ends_with(tag.name) {
            return Some(tag);
        }
    }
    data.tags.iter().filter(|tag| value.ends_with(tag.name)).next()
}
/// Tag definitions missing in the API registry.
const TAGS: &[Tag] = &[Tag { name: "KHX", author: "<none>", contact: "<none>" }];

/// Removes an defined tag present from a value.
pub fn remove_tag<'a>(mut value: &'a str, data: &Data) -> &'a str {
    if let Some(tag) = find_tag(value, data) {
        value = remove_suffix(value, &[tag.name, "_"]);
    }
    value
}

/// Formats an `i32` value to be converted to a `u32` value in the generated code.
pub fn format_value(value: i32) -> String {
    if value < 0 {
        format!("{} as i32 as u32", value)
    } else {
        format!("{}", value as u32)
    }
}

/// Formats an `i32` value to be converted to a `u32` value in the generated code.
/// Positive values are formatted with radix 16.
pub fn format_value_hex(value: i32) -> String {
    if value < 0 {
        format!("{} as i32 as u32", value)
    } else {
        format!("0x{:x}", value as u32)
    }
}

/// Maps an API registry type to a valid rust type.
pub fn map_type(name: &str) -> &str {
    match name {
        "int8_t" => "i8",
        "uint8_t" => "u8",
        "int16_t" => "i16",
        "uint16_t" => "u16",
        "int32_t" => "i32",
        "uint32_t" => "u32",
        "int64_t" => "i64",
        "uint64_t" => "u64",
        "size_t" => "usize",
        "int" => "i32",
        "float" => "f32",
        "char" => "c_char",
        "void" => "c_void",
        x => x,
    }
}

/// Maps an API registry name to a valid rust name.
pub fn map_name(name: &str) -> &str {
    match name {
        "type" => "ty",
        x => x,
    }
}
