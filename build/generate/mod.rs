/// Macro to output generated rust code.
///
/// The first argument `target` is a `Target` structure containing the necessary facilities for
/// writing  the generated code.
///
/// The remaining arguments are passed to the `write` macro used.
macro_rules! out {
    ($target:ident, $($args:tt)*) => (write!($target.file, $($args)*).unwrap())
}
/// Macro to output a generated line of rust code.
///
/// The first argument `target` is a `Target` structure containing the necessary facilities for
/// writing  the generated code.
///
/// The remaining arguments are passed to the `writeln` macro used.
macro_rules! outln {
    ($target:ident, $($args:tt)*) => (writeln!($target.file, $($args)*).unwrap())
}


mod utils;
mod basic;
mod ty;
mod cn;
mod ext;
mod en;
mod bm;
mod func;
mod st;
mod cmd;


use {ContextGenerate, Target};
use std::io::Write;

/// Generates the API binding.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    outln!(target, "");
    ty::generate_basetypes(ctx, target);
    outln!(target, "");
    ty::generate_handles(ctx, target);

    outln!(target, "");
    cn::generate(ctx, target);

    outln!(target, "");
    ext::generate(ctx, target);

    outln!(target, "");
    en::generate(ctx, target);
    ty::generate_enums(ctx, target);
    outln!(target, "");
    bm::generate(ctx, target);
    ty::generate_bitmasks(ctx, target);

    outln!(target, "");
    func::generate(ctx, target);

    outln!(target, "");
    st::generate(ctx, target);

    outln!(target, "");
    cmd::generate(ctx, target);
}
