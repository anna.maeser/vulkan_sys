use super::utils;
use {ContextGenerate, Target};
use std::io::Write;
use utils::*;

/// Generates all base types if not denied / protected.
pub fn generate_basetypes(ctx: &ContextGenerate, target: &mut Target) {
    use parse::ty::Category;

    for ty in ctx.data
        .types
        .iter()
        .filter(|ty| ty.category == Category::BaseType)
        .filter(|ty| !utils::check_unavailable_type(ctx, ty.name))
    {
        outln!(target, "pub type {} = {};", remove_prefix(ty.name, &["Vk"]), utils::map_type(ty.ty.expect("type/@type")));
    }
}

/// Generates all handles if not denied / protected.
pub fn generate_handles(ctx: &ContextGenerate, target: &mut Target) {
    use parse::ty::Category;

    for handle in ctx.data
        .types
        .iter()
        .filter(|ty| ty.category == Category::Handle)
        .filter(|ty| !utils::check_unavailable_type(ctx, ty.name))
    {
        outln!(
            target,
            "pub type {} = {};",
            remove_prefix(handle.name, &["Vk"]),
            match handle.ty.expect("type/@type") {
                "VK_DEFINE_HANDLE" => "usize",
                "VK_DEFINE_NON_DISPATCHABLE_HANDLE" => "u64",
                x => panic!("unknown handle type: {}", x),
            }
        );
    }
}

/// Generates all enumeration types if not already handled by enumeration generation
/// and not denied / protected
pub fn generate_enums(ctx: &ContextGenerate, target: &mut Target) {
    use parse::ty::Category;

    for ty in ctx.data
        .types
        .iter()
        .filter(|ty| ty.category == Category::Enum)
        .filter(|ty| !ctx.data.enums.iter().any(|en| en.name == ty.name))
        .filter(|ty| !utils::check_unavailable_type(ctx, ty.name))
    {
        outln!(target, "pub type {} = {};", remove_prefix(ty.name, &["Vk"]), "u32");
    }
}

/// Generates all bit mask types if not already handled by bit mask generation
/// and not denied / protected
pub fn generate_bitmasks(ctx: &ContextGenerate, target: &mut Target) {
    use parse::en::Type;
    use parse::ty::Category;

    for ty in ctx.data
        .types
        .iter()
        .filter(|ty| ty.category == Category::Bitmask)
        .filter(|ty| {
            ty.requires.is_none() ||
                !ctx.data
                    .enums
                    .iter()
                    .filter(|en| en.ty == Type::Bitmask)
                    .any(|en| en.name == ty.requires.unwrap())
        })
        .filter(|ty| !utils::check_unavailable_type(ctx, ty.name))
    {
        outln!(
            target,
            "pub type {} = {};",
            remove_prefix(ty.name, &["Vk"]),
            match ty.ty {
                Some(ty) => remove_prefix(ty, &["Vk"]),
                _ => "u32",
            }
        );
    }
}
