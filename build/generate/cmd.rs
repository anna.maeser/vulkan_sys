use super::{basic, utils};
use {ContextGenerate, Target};
use parse::cmd::Command;
use std::io::Write;
use utils::*;

/// Generates the commands if not denied / protected.
///
/// Commands are not generated directly but through the `commands` macro which gets all the
/// necessary information to finally build a structure containing all function pointers
/// to the commands and the facility to obtain and set the actual poiter values.
///
/// Commands are split up into different categories depending on how their pointer is obtained.
/// 1. Exported by the Vulkan loader shared library
/// 1. Global / static pointers provided by the `GetInstanceProdAddr` function
/// 1. Instance specific pointers
/// 1. Device specific pointers
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    generate_category(ctx, target, "CommandsExported", |command| is_exported(command));
    generate_category(ctx, target, "CommandsGlobal", |command| is_global(command));
    generate_category(ctx, target, "CommandsInstance", |command| is_instance(command));
    generate_category(ctx, target, "CommandsDevice", |command| is_device(command));
}

/// Tells if the command is an exported pointer.
fn is_exported(command: &Command) -> bool {
    command.name == "vkGetInstanceProcAddr"
}

/// Tells if the command is a globally provided pointer.
fn is_global(command: &Command) -> bool {
    !is_exported(command) && !is_instance(command) && !is_device(command)
}

/// Tells if the command is an instance specific pointer.
fn is_instance(command: &Command) -> bool {
    command.name == "vkGetDeviceProcAddr" ||
        command.name != "vkGetInstanceProcAddr" &&
            if let Some(p1) = command.parameters.iter().nth(0) {
                p1.ty.name == "VkInstance" || p1.ty.name == "VkPhysicalDevice"
            } else {
                false
            }
}

/// Tells if the command is a device specific pointer.
fn is_device(command: &Command) -> bool {
    command.name != "vkGetDeviceProcAddr" &&
        if let Some(p1) = command.parameters.iter().nth(0) {
            p1.ty.name == "VkDevice" || p1.ty.name == "VkQueue" || p1.ty.name == "VkCommandBuffer"
        } else {
            false
        }
}

/// The actual generation of the commands macro invocation.
fn generate_category<F>(ctx: &ContextGenerate, target: &mut Target, label: &str, filter: F)
    where F: FnMut(&&Command) -> bool
{
    outln!(target, "commands!({}, {{", label);
    for command in ctx.data
        .commands
        .iter()
        .filter(filter)
        .filter(|command| !utils::check_unavailable_command(ctx, command.name))
    {
        out!(target, "    {} => (", remove_prefix(command.name, &["vk"]));
        for (index, parameter) in command.parameters.iter().enumerate() {
            basic::generate_parameter(target, parameter, index);
        }
        out!(target, ") -> ");
        basic::generate_return(target, &command.ret);
        outln!(target, ",");
    }
    outln!(target, "}});");
}
