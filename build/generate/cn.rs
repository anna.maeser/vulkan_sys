use super::utils;
use {ContextGenerate, Target};
use std::io::Write;
use utils::*;

/// Generates the constant definitions if not denied / protected.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    for con in ctx.data.consts.iter().filter(|ty| !utils::check_unavailable_type(ctx, ty.name)) {
        let (ty, value) = map_value(con.value);
        outln!(target, "pub const {}: {} = {};", remove_prefix(con.name, &["VK_"]), ty, value);
    }
}

/// Maps a value from the API registry into a rust conform value expression.
fn map_value(mut string: &str) -> (&str, String) {
    if string.ends_with("f") {
        let len = string.len();
        return ("f32", string[..len - 1].to_owned());
    }
    if string.starts_with("(") && string.ends_with(")") {
        let len = string.len();
        string = &string[1..len - 1];
    }
    let is_long = if string.ends_with("LL") {
        let len = string.len();
        string = &string[..len - 2];
        true
    } else {
        false
    };

    (if is_long { "u64" } else { "u32" }, string.to_owned().replace("U", "").replace("~", "!"))
}
