use super::{basic, utils};
use {ContextGenerate, Target};
use parse::basic::Member;
use parse::st::Struct;
use std::io::Write;
use utils::*;

/// Generates structure definitions if not denied / protected.
///
/// Structures are not generated directly but through the `structure` macro which gets all the
/// necessary information to finally build a structure with all the convenience necessary.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    for st in ctx.data
        .structs
        .iter()
        .filter(|st| !utils::check_unavailable_type(ctx, st.name))
    {
        let (pointer, sized) = find_markers(ctx, st);
        outln!(target, "structure!(");
        if !pointer && !sized {
            outln!(target, "    #[derive(Clone, Copy)]");
            outln!(target, "    #[derive(Debug)]");
        }
        outln!(target, "    {}, {{", remove_prefix(st.name, &["Vk"]));
        for member in st.members.iter() {
            generate_member(ctx, target, member);
        }
        outln!(target, "    }}");
        outln!(target, ");")
    }
}

fn find_markers(_ctx: &ContextGenerate, st: &Struct) -> (bool, bool) {
    st.members.iter()
        .map(|m| &m.ty)
        .map(|ty| (
            ty.pointer || ty.const_ptr,
            ty.size.is_some()))
        .fold((false, false), |result, next| (
            result.0 || next.0,
            result.1 || next.1))
}

/// Generates a member of a structure.
fn generate_member(_ctx: &ContextGenerate, target: &mut Target, member: &Member) {
    out!(target, "        {}: ", utils::map_name(member.name));
    basic::generate_type(target, &member.ty);
    outln!(target, ",");
}
