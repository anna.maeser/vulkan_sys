/// Removes multiple prefixes from a string value.
///
/// If a single prefix is not present at the time of checking it is just skipped.
///
/// # Examples
///
/// ```
/// assert_eq!("GetValue", remove_prefix("PFN_vkGetValue", &["PFN_", "vk"]));
/// assert_eq!("vkGetValue", remove_prefix("PFN_vkGetValue", &["vk", "PFN_"]));
/// ```
pub fn remove_prefix<'a>(mut value: &'a str, prefixes: &[&str]) -> &'a str {
    for prefix in prefixes {
        if value.starts_with(prefix) {
            value = &value[prefix.len()..];
        }
    }
    value
}

/// Removes multiple suffixes from a string value.
///
/// If single suffix is not present at the time of checking it is just skipped.
///
/// # Examples
///
/// ```
/// assert_eq!("Value", remove_suffix("ValueFlagBit", &["Bit", "Flag"]));
/// assert_eq!("ValueFlag", remove_suffix("ValueFlagBit", &["Flag", "Bit"]));
/// ```
pub fn remove_suffix<'a>(mut value: &'a str, suffixes: &[&str]) -> &'a str {
    for suffix in suffixes {
        if value.ends_with(suffix) {
            value = &value[..value.len() - suffix.len()];
        }
    }
    value
}
