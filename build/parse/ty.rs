use super::{utils, xpath};
use ContextExtract;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// The type categories.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Category {
    /// A reference to a type.
    Reference,
    /// Defines a base type.
    BaseType,
    /// Defines a handle.
    Handle,
    /// Defines a bit mask.
    Bitmask,
    /// Defines an enumeration
    Enum,
    /// Defines a structure.
    Struct,
    /// Defines a function pointer.
    FunctionPointer,
    /// Defines a union.
    Union,
    /// Defines a #define
    Define,
    /// Defines an #include
    Include,
}

/// A type definition.
pub struct Type<'a> {
    /// The category of the definition.
    pub category: Category,
    /// The name of the type.
    pub name: &'a str,
    /// The optional type of the definition.
    pub ty: Option<&'a str>,
    /// The optional parent of the type.
    pub parent: Option<&'a str>,
    /// The optional requirement of the tpye.
    pub requires: Option<&'a str>,
    /// If `true` the type will only be returned by commands.
    pub returned_only: bool,
}

impl<'a> Debug for Type<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        if self.ty.is_some() {
            write!(fmt, "{:?} > {}: {}", self.category, self.name, self.ty.as_ref().unwrap())
        } else {
            write!(fmt, "{:?} > {}", self.category, self.name)
        }
    }
}

/// Lists the type definitions specified in the API registry.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Type<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/types/type")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .filter_map(|element| map(element))
        .collect()
}

/// Maps a `type` element into a `Type` structure.
fn map(element: Element) -> Option<Type> {
    use std::str::FromStr;

    let category = if let Some(category) = map_category(element.attribute_value("category")) {
        category
    } else {
        return None;
    };
    let name = utils::element_child_text(element, "name")
        .or_else(|| element.attribute_value("name"))
        .expect("type/@name");
    let ty = match category {
        Category::FunctionPointer => None,
        _ => utils::element_child_text(element, "type"),
    };

    Some(Type {
        category: category,
        name: name,
        ty: ty,
        parent: element.attribute_value("parent"),
        requires: element.attribute_value("requires"),
        returned_only: element
            .attribute_value("returnedonly")
            .map(|s| FromStr::from_str(s).unwrap())
            .unwrap_or(false),
    })
}

/// Maps the `category` attribute value to a `Category` variant.
fn map_category(cat: Option<&str>) -> Option<Category> {
    Some(match cat {
        Some(cat) => {
            match cat {
                "basetype" => Category::BaseType,
                "handle" => Category::Handle,
                "bitmask" => Category::Bitmask,
                "enum" => Category::Enum,
                "struct" => Category::Struct,
                "funcpointer" => Category::FunctionPointer,
                "union" => Category::Union,
                "define" => Category::Define,
                "include" => Category::Include,
                x => panic!(format!("unknown type category {}", x)),
            }
        }
        None => Category::Reference,
    })
}
