use super::{utils, xpath};
use super::basic::{Member, Type};
use ContextExtract;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// A structure definition.
pub struct Struct<'a> {
    /// The name of the structure.
    pub name: &'a str,
    /// If `true` the structure is only returned by commands.
    pub returned_only: bool,
    /// The members of the structure.
    pub members: Vec<Member<'a>>,
}

impl<'a> Debug for Struct<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{} -> {:?}", self.name, self.members)
    }
}

/// Lists the structure definitions specified in the API registry.
///
/// A structure is defined in a `type` element with a `category` attribute value of `"struct"`.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Struct<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/types/type[@category = 'struct']")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map(element))
        .collect()
}

/// Maps a `type` element into a `Struct` structure.
fn map(element: Element) -> Struct {
    use std::str::FromStr;

    let members = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("member"))
        .map(|e| map_member(e))
        .collect();

    Struct {
        name: element.attribute_value("name").expect("type/@name"),
        returned_only: element
            .attribute_value("returnedonly")
            .and_then(|v| bool::from_str(v).ok())
            .unwrap_or(false),
        members: members,
    }
}

/// Maps a `member` element into a `Member` structure.
fn map_member(element: Element) -> Member {
    let ty_element = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("type"))
        .next()
        .expect("member/type");
    let ty = ty_element
        .children()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .expect("member/type");
    let constant = ty_element
        .preceding_siblings()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .map(|t| t.trim() == "const")
        .unwrap_or(false);
    let following = ty_element
        .following_siblings()
        .into_iter()
        .filter_map(utils::child_text())
        .next();
    let pointer = following.map(|t| t.contains("*")).unwrap_or(false);
    let const_ptr = following.map(|t| t.contains("const*")).unwrap_or(false);

    Member {
        name: utils::element_child_text(element, "name").expect("member/name"),
        ty: Type {
            name: ty,
            pointer: pointer,
            constant: constant,
            size: utils::element_child_text(element, "enum"),
            const_ptr: const_ptr,
        },
    }
}
